# A SGM8302 dual channel amplifier for VHS decode RF capture

NOTE this design has been superseded with [a new one over here](https://gitlab.com/wolfre/vhs-rf-amp-ada4857) that uses easier to obtain parts and has better performance.

![render-1.0.png](render-1.0.png)

An [SGM8302][sgm8302-datasheet] based, configurable, dual channel amplifier and line driver for [VHS decode][vhs-decode] or other RF hackery.
Find gerbers ready for fabrication over in the [releases section](https://gitlab.com/wolfre/vhs-rf-amp-sgm8302/-/releases).
To build the amp you need to choose the appropriate components for your VHS recorder / project, read on to find out more.

## Overview

The design is based on an [SGM8302][sgm8302-datasheet] dual channel op amp in an easy to solder SOIC-8 package, with other components being either 0805 or through hole parts.
The [SGM8302][sgm8302-datasheet] is a high speed operational amplifiers that is build for video applications with -0.1dB at 56MHz.
The design has a ±5V LDO power supply circuit which should give optimal performance, but the amps can operate from ±2.25V to ±6V dual supplies as well.

The design features two identical signal paths.
Each of the inputs is being AC coupled into a termination resistor.
The op amp is used in a non-inverting design with a fixed gain factor (can be chosen according to you needs, see later).
The outputs have a resistor in series, for the usual 75 Ohms line and load to drive (but can be changed to 50 Ohms as well).
The RF paths should be about 75 Ohms impedance, with a 1.2 mm FR4 prepreg.
Mechanically the board is designed for [edge mount SMA connectors](https://www.google.com/search?q=sma+connector+edge+mount).

## Adapting the amp inputs to your VHS player

The inputs are AC coupled via C2 + R3 and C1 + R4.
Both caps can be 1µF, but 3.3µF is better.
Selecting the resistors is more complicated as they need to work with the tape head amp output on [your RF tap][rf-tap].
Here's a rough guide on how to do this with some try'n'error (having a common resistor set at hand is a good idea).

First find an [RF tap][rf-tap] in your VHS machine and solder / install cables to it.
If you have it, take a look at the service manual, and look for resistors around the signal path you tapped.
Choose a value that is around 10x higher than the values you observe (e.g. 1.5k is around the signal path, go for a 15k Ohms resistor), but to be safe *don't go below 1k Ohms*.
Hook up a [DSO][wiki-dso] to the output of your tap and observe the signal levels.
Temporarily attach the chosen resistor value between signal and (analog) ground to see if the levels drop.
If they stay the same then repeat with a resistor value that is about half your current value, if they do drop, double the value.
You are looking for a value where the signal is just barely dropping, multiply that value by about 5, that's your input termination.

If you don't have the service manual you can just start with about 50k Ohms.
If you don't have a [DSO][wiki-dso] then a 20~50k Ohms is probably fine.

One of the signal paths is going to be for video, the other for audio.
Which one is which does not matter, but you should do the above try'n'error sequence for both head amps as they may differ.
This will get you the values for R3 and R4.

## Adapting the amp gain to your VHS player

Once you have your input stage setup, you need to choose the gain for each signal path.
Make sure to observe the video and audio RF signal levels from your VHS head amps with different VHS tapes.
Different tapes will have different signal levels (test newer and older tapes, NTSC and PAL, etc).
Consulting the service manual for your player may also give you a good idea about the expected signal levels.
Based on this you now need to choose the values for R5 + R6 (path A) and R7 + R8 (path B).

You should also know what your target capture device needs as a good input range.
A [CXADC](https://github.com/happycube/cxadc-linux3) seems to be [fine with about 1.5Vpp](https://gitlab.com/wolfre/cx25800-11z-cxadc-rework-measurements) (peak-to-peak).

So lets say you observe 0.5Vpp (500mVpp) of signal level and your device is a CX based capture card, so a target of 1.5Vpp.
This means we need 6x gain, here's why: from 0.5V to 1.5V is a 3x gain, but you also need to have [2x for the transmission line](https://electronics.stackexchange.com/questions/550000/50-ohms-output-termination-for-a-50ohms-load), 2 x 3 gives a gain of 6.
The following tables lists good combinations for the resistor pairs from the [E12](https://en.wikipedia.org/wiki/E_series_of_preferred_numbers) series that also amount to about 400 Ohms as per SGM8302 data sheet suggestion.
In doubt select the next lower gain to get more headroom on the ADC input.

|Rf (Ohms) | Rin (Ohms) | gain | total resistance (Ohms)|
|----------|------------|------|------------------------|
|  68      |        330 | 1.20 | 398                    |
|  82      |        330 | 1.24 | 412                    |
| 100      |        330 | 1.30 | 430                    |
| 100      |        270 | 1.37 | 370                    |
| 120      |        270 | 1.44 | 390                    |
| 150      |        270 | 1.55 | 420                    |
| 150      |        220 | 1.68 | 370                    |
| 180      |        220 | 1.81 | 400                    |
| 220      |        220 | 2.00 | 440                    |
| 220      |        180 | 2.22 | 400                    |
| 270      |        180 | 2.50 | 450                    |
| 270      |        150 | 2.80 | 420                    |
| 270      |        120 | 3.25 | 390                    |
| 270      |        100 | 3.70 | 370                    |
| 330      |        100 | 4.30 | 430                    |
| 330      |         82 | 5.02 | 412                    |
| 330      |         68 | 5.85 | 398                    |
| 330      |         56 | 6.89 | 386                    |
| 330      |         47 | 8.02 | 377                    |
| 390      |         47 | 9.29 | 437                    |

If you need more gain you may exceed the working range of the amp, so better consult the [data sheet][sgm8302-datasheet].

The above table lists Rf and Rin values, the mapping is as follows:
- Path A
  - Rf = R6
  - Rin = R5
- Path B
  - Rf = R7
  - Rin = R8

As mentioned previously the design of the board should fit a 75 Ohms impedance / transmission line and termination.
This is the default found on CX based capture cards and in other video based applications.
However this can be changed by swapping the series resistors R1 and/or R2 to a different value.
[SGM8302][sgm8302-datasheet] can just as well drive a 50 Ohms line / 100 Ohm load.

## Power supply section

The power supply section consists of two [LDOs][wiki-ldo] that will dissipate the excess voltage drop as heat.
Through ineffective, this should provide for a low noise supply to the op amp.
The design works with a L7905 (-5V) and L7805 (+5V) from a wide input voltage range.
VHS recorders tend to have rather "high" voltages running around (e.g. -20V, +25V) and these regulators can cope with the huge voltage drop.
If powering one of the [LDOs][wiki-ldo] from more than about 25V may require a small heat-sink to remove the excess heat.
The [SGM8302][sgm8302-datasheet] can operate from asymmetric power supplies, which means the power up sequence of the two source voltages in your VHS deck does not matter too much.
If you happen to have a good, stabilized, 5V in your machine, you can directly power the amp from that as well (and leave out the respective [LDO(s)][wiki-ldo]).
When looking for a ground to power the amp, make sure to double check that this is the same ground as the one from your head amp signal (multimeter reads 0 Ohm between the two grounds).

## Final words

![sony-svo-1500p-install.jpg](sony-svo-1500p-install.jpg)

Here's a picture of my final build inside a Sony SVO-1500p (slightly earlier board revision) with a 3D printed holder.
It was fun building it and even if the [SGM8302][sgm8302-datasheet] may not be available in your region, maybe the above helps you to find and build an alternative that works for you.
Happy RF capture.

[sgm8302-datasheet]: https://www.sg-micro.com/uploads/soft/20211229/1640769028.pdf
[vhs-decode]: https://github.com/oyvindln/vhs-decode
[rf-tap]: https://github.com/oyvindln/vhs-decode/wiki/Hardware-Installation-Guide
[wiki-dso]: https://en.wikipedia.org/wiki/Digital_storage_oscilloscope
[wiki-ldo]: https://en.wikipedia.org/wiki/Low-dropout_regulator
